<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>All4Class</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/business-frontpage.css" rel="stylesheet">

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="/">Inicial
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('mapa') }}">Mapa</a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="{{ route('participe') }}">Participe</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('login') }}">Login</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>        
  <div class="container">
      <h2>Solicitação de <i>abertura de diálogo</i></h2>
        <form method="post" action="{{route('enviar_requisicao')}}">        
        {{ csrf_field() }}
        <?php
          use App\Escola;
          use App\Instituicoes;
          $escolas = Escola::all();
          $Inst    = Instituicoes::all();
        ?>
        <br>
        
        <h4>Nome</h4>
        <input class="form-control" type="text" name="nomeResponsavel">
        <br>

        <h4>Telefone</h4>
        <input class="form-control" type="text" name="telefone">
        <br>

        <h4>Email</h4>
        <input class="form-control" type="email" name="email">
        <br>

        <h4>Selecione a escola</h4>
        <select class="form-control" name="escola_id">
        @foreach($escolas as $e)
          <option value="{{ $e->id }}">{{ $e->nome }}</option>
        @endforeach
        </select>
        <br>

        <h4>Selecione a instituição</h4>
        <select class="form-control" name="instituicao_id">
        @foreach($Inst as $i)
          <option value="{{ $i->id }}">{{ $i->nome }}</option>
        @endforeach
        </select>
        <br>
        <h4>Selecione o perfil de aluno</h4>
        <select class="form-control" name="perfilAluno">
          <option value="Vulnerável à drogas e tráfico">Vulnerável às drogas e tráfico</option>
          <option value="Vulnerável à prostituição">Vulnerável à prostituição</option>
          <option value="Vulnerável ao trabalho infantíl">Vulnerável ao trabalho infantíl </option>
          <option value="Vulnerável ao violência doméstica">Vulnerável à violência doméstica</option>
          <option value="Vulnerável ao furto para subexistência">Vulnerável ao furto para subexistência</option>
        </select>
        <br>
        <br>
        <h4>Descrição da problemática</h4>
          <textarea name="requisicao" class="form-control" id="" cols="30" rows="10"></textarea>
          <br>
          <input type="submit" class="btn btn-success" value="enviar">
          <br><br>
        </form>
    </div>
     <!-- Header with Background Image -->
    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Class4All 2018</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>

</html>
