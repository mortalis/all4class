@extends('adminlte::page')

@section('title', 'Resposta')

@section('content_header')
    <h1>Dados da Requisição <b> {{ $requisicoes->id }}</b></h1>
@endsection

@section('content')
<div class="container">
  <div class="row">
    <div class="col-sm-4">
      <h3>Dados da Requisição</h3>
      <b>Autor: </b> {{ $requisicoes->nomeResponsavel }}<br>
      <b>E-mail: </b> {{ $requisicoes->email }}<br>
      <b>Telefone: </b> {{ $requisicoes->telefone }} <br>
      <b>Descrição: </b> {{ $requisicoes->requisicao }} <br>
      <br>
      
    </div>
    <div class="col-sm-6">
      <h3>Responder</h3>
      <form action="{{route('resposta')}}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="id" value="{{ $requisicoes->id }}" s>
        <div class="form-group">
          <label for="nome">Nome do Autor:</label>
          <input id="nome" name="nome" type="text" class="form-control" value="{{ $requisicoes->nomeResponsavel }}" readonly>
        </div>
        <div class="form-group">
          <label for="email">E-mail:</label>
          <input id="email" name="email" type="text" class="form-control" value="{{ $requisicoes->email }}" readonly>
        </div>
        <div class="form-group">
          <label for="email">Telefone:</label>
          <input id="telefone" name="telefone" type="text" class="form-control" value="{{ $requisicoes->telefone }}" readonly>
        </div>
        <div class="form-group">
          <label for="mensagem">Resposta:</label>
          <textarea rows="4" cols="50" id="mensagem" name="mensagem" class="form-control"></textarea>
        </div>
        <input type="submit" class="btn btn-success" value="enviar">
      </form>
    </div>
  </div>
</div>

@endsection