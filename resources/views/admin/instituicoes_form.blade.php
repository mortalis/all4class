@extends('adminlte::page')

@section('title', 'Cadastro de Instituições')

@section('content')

    <div class='col-sm-11'>
        
        <h2> Inclusão de Instituições </h2>
    
    </div>
    <div class='col-sm-1'>
        <a href="{{route('instituicoes.index')}}" class="btn btn-primary"
           role="button">Voltar</a>
    </div>

    <div class='col-sm-12'>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        
            <form method="post" action="{{route('instituicoes.store')}}">
                
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="nome">Nome da Instituição:</label>
                    <input type="text" class="form-control" id="nome"
                            name="nome"
                            
                            required>
                </div>

                <div class="form-group">
                        <label for="nome">Telefone:</label>
                        <input type="text" class="form-control" id="telefone"
                                name="telefone"
                                
                                required>
                </div>

                <div class="form-group">
                        <label for="nome">Endereço:</label>
                        <input type="text" class="form-control" id="endereco"
                                name="endereco"
                                
                                required>
                </div>


                        <button type="submit" class="btn btn-primary">Enviar</button>
                        <button type="reset" class="btn btn-warning">Limpar</button>
                    </form>
    </div>


@endsection