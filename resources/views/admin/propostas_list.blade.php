@extends('adminlte::page')

@section('title', 'Lista de Requisições')

@section('content')

    <div class='col-sm-12'>
        <h2> Requisições </h2>
    </div>
    @if (Session::has('success'))
	<div class="alert alert-success" role="alert">
		<strong>Sucesso:</strong> {{ Session::get('success') }}
	</div>
    @elseif (Session::has('error'))
	<div class="alert alert-danger" role="alert">
		<strong>Erro:</strong> {{ Session::get('error') }}
	</div>
    @endif

@if (count($errors) > 0)
	<div class="alert alert-danger" role="alert">
		<strong>Erros:</strong> 
		<ul>
		@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
		</ul>
	</div
@endif
    <div class='col-sm-12'>
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <table class="table table-striped">
            <tr>
                <th>Código</th>
                <th>Autor da Requisição</th>
                <th>Email</th>
                <th>Telefone</th>
                <th>Descrição</th>
                <th>Escola</th>
                <th>Perfil dos Alunos</th>
                <th>Corporação acionada</th>
                <th>Responder</th>
                
            </tr>
            @forelse($requisicoes as $r)
                <tr>
                    <td style="text-align: center">{{$r->id}}</td>
                    <td>{{$r->nomeResponsavel}}</td>
                    <td>{{$r->email}}</td>
                    <td>{{$r->telefone}}</td>
                    <td>{{$r->requisicao}}</td>
                    <td>{{$r->escola->nome}}</td>
                    <td>{{$r->perfilAluno}}</td>
                    <td>{{$r->instituicao->nome}}</td>
                    <td> <a href="{{route('requisicoes.resposta', $r->id)}}"
                        class="btn btn-info"
                        role="button">Responder</a> &nbsp;&nbsp;</td>
                </tr>
            @empty

                <h4>Não existem proposta cadastradas ainda.</h4>

            @endforelse
        </table>
        
    </div>

@endsection