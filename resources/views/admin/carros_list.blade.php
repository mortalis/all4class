@extends('adminlte::page')

@section('title', 'Cadastro de Escolas')

@section('content_header')
    <h1>Cadastro de Escolas
    <a href="{{ route('escolas.create') }}" 
       class="btn btn-primary pull-right" role="button">Novo</a>
    </h1>
@endsection

@section('content')

@if (session('status'))
    <div class="alert alert-success">
      {{ session('status') }}
    </div>  
@endif

<table class="table table-striped">
  <tr>
    <th> Nome </th>
    <th> Bairro </th>
    <th> Endereço </th>
    <th> Ações </th>
  </tr>  
@forelse($escolas as $e)
  <tr>
    <td> {{$e->nome}} </td>
    <td> {{$e->bairro->nome}} </td>
    <td> {{$e->endereco}} </td>
  
    <td> 
    
        
        <form style="display: inline-block"
              method="post"
              action="{{route('escolas.destroy', $e->id)}}"
              onsubmit="return confirm('Confirma Exclusão?')">
               {{method_field('delete')}}
               {{csrf_field()}}
              <button type="submit" title="Excluir"
                      class="btn btn-danger btn-sm"><i class="far fa-trash-alt"></i></button>
        </form>  &nbsp;&nbsp;
        
    </td>
  </tr>

@empty
  <tr><td colspan=8> Não há Escolas cadastradas ou filtro da pesquisa não 
                     encontrou registros </td></tr>
@endforelse
</table>

@endsection

@section('js')
  <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
@endsection