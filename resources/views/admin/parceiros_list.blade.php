@extends('adminlte::page')

@section('title', 'Cadastro de Parceiros')

@section('content_header')
    <h1>Cadastro de Instituições
    <a href="{{ route('instituicoes.create') }}" 
       class="btn btn-primary pull-right" role="button">Novo</a>
    </h1>
@endsection

@section('content')

@if (session('status'))
    <div class="alert alert-success">
      {{ session('status') }}
    </div>  
@endif

<table class="table table-striped">
  <tr>
    <th> Nome </th>
    <th> Telefone </th>
    <th> Endereço </th>
    <th> Ações </th>
  </tr>  
@foreach($instituicoes as $i)
  <tr>
    <td> {{$i->nome}} </td>
    <td> {{$i->telefone}} </td>
    <td> {{$i->endereco}} </td>
    <td> 
    
        
        <form style="display: inline-block"
              method="post"
              action="{{route('instituicoes.destroy', $i->id)}}"
              onsubmit="return confirm('Confirma Exclusão?')">
               {{method_field('delete')}}
               {{csrf_field()}}
              <button type="submit" title="Excluir"
                      class="btn btn-danger btn-sm"><i class="far fa-trash-alt"></i></button>
        </form>  &nbsp;&nbsp;
        
    </td>
  </tr>
  
@endforeach
</table>
{{ $instituicoes->links() }}
@endsection

@section('js')
  <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
@endsection