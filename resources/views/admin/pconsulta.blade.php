@extends('adminlte::page')

@section('title', 'Cadastro de Parceiros')

@section('content_header')
    <h1>Consulta de Parceiros</h1>

@endsection

@section('content')

@if (session('status'))
    <div class="alert alert-success">
      {{ session('status') }}
    </div>  
@endif

<form method="POST" action="{{route('parceiros.search')}}" enctype="multipart/form-data">

    {{csrf_field()}}

    <div class="row">
        <div class="col-sm-3"> 
            <div class="form-group"> 
                <label for="nome"> Digite a cidade que deseja pesquisar </label>
                <input type="text" id="cidade" name="cidade" required class="form-control">
            </div>
        </div>
    </div>
    <input type="submit" value="Consultar" class="btn btn-success">
    <input type="reset" value="limpar" class="btn btn-warning">
</form>

<br>

<h4> Gerar PDF com todos os Parceiros </h4>
<a href="/relparceiros">Todos os parceiros</a>

@endsection

@section('js')
  <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
@endsection