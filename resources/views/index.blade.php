<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>All4Class</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/business-frontpage.css" rel="stylesheet">

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="/">Inicial
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('mapa') }}">Mapa</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('participe') }}">Participe</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('login') }}">Login</a>
            </li>            
          </ul>
        </div>
      </div>
    </nav>

    <!-- Header with Background Image -->
    <div class="jumbotron" style="height:600px; background: url('http://www.politize.com.br/wp-content/uploads/2016/06/politize-cidadania.jpeg')">
      <font color=white>
        <h1>All4Class<font color=grey> //</font> Pelotas</h1>
      </font>
      <hr>
      <h4><i>Uma sociedade mais segura se constroi com cidadania, educação e ação coletiva</i></h4>
    </div>
    <!-- Page Content -->
    <div class="container">

      <div class="row">
        <div class="col-sm-12">
          <h2 class="mt-4">O que fazemos?</h2>
          <p style="text-align: justify">O projeto <b>All4Class</b> tem por objetivo aproximar as escolas públicas, sobretudo aquelas que atendem majoritariamente estudantes
            em condição de vulnerabilidade social, das entidades públicas competentes, de modo que estas atuem de forma direta na construção da consciência cidadã destas crianças. Estas integração
            possibilita que a própria sociedade manifeste suas demandas e anseios quanto ao processo de formação destas crianças, permitindo que pais, educadores e outros membros da sociedade civil
            solicitem atuação direcionada de modo a garantir o seu bem-estar.
          </p>
            <a class="btn btn-primary btn-lg" href="">Seja parte da mudança!</a>
          </p>
        </div>
       </div>
      <!-- /.row -->

      <div class="row">
        <div class="col-sm-6">
          <div class="card">
            <img class="card-img-top" src="https://3.bp.blogspot.com/-guyPtZeTQbg/V2tkphgoLyI/AAAAAAAANgc/SSsjrRYqeCUi4ymVVYHxx8aGsZevKLFDACLcB/s640/01.jpg" alt="">
            <div class="card-body">
              <h4 class="card-title" style="text-align: center">Vulnerabilidade</h4>
              <p class="card-text" style="text-align:justify">
                A Vulnerabilidade social é um dos principais fatores de risco para criminalidade,
                tendo impacto extremamente nocivo na vida de muitas crianças em idade escolar. As
                dificuldades na subesistễncia, muitas vezes atrelada à uma condição familar 
                conturbada, podem ser determinadas na formação do caráter destas pessoas,
                bem como nas escolhas quanto ao seu futuro. 
              </p>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="card">
            <img class="card-img-top" src="http://www.suacidade.com/sites/default/files/images/policia_escola.jpg" alt="">
            <div class="card-body">
              <h4 class="card-title" style="text-align: center">Descubra as demandas</h4>
              <p class="card-text" style="text-align: justify">
                Através de nossa plataforma a sociedade civil pode trazer demandas para diversas
                entidades públicas, como Policia Civil, Policia Militar e Conselho Tutelar, atuarem
                de forma direta em escolas públicas de modo a sanar problemas enfrentados por alunos
                em condição de vulnerabilidade social. 
              </p>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <br><br>
        </div>
      </div>
      </div>
    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Class4All 2018</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>

</html>
