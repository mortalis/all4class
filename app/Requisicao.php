<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Requisicao extends Model
{
    protected $fillable = ['nomeResponsavel', 'email', 'telefone', 'requisicao', 'escola_id', 'perfilAluno', 'instituicao_id'];
    public $table = "requisicao";
    public function escola() {
        return $this->belongsTo('App\Escola', 'escola_id');
    }

    public function instituicao(){
        return $this->belongsTo('App\Instituicoes', 'instituicao_id');
    }

    
}
