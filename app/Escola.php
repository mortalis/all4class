<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Escola extends Model
{
    protected $fillable = ['nome', 'bairro_id', 'endereco'];

    public $table = "escola";
    public function bairro() {
        return $this->belongsTo('App\Bairro', 'bairro_id');
    }

    // define os campos que serão editados na inclusão/alteração 
    // pelos métodos create e update
    

    
    
}
