<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Carro;
use App\Parceiros;

class wsController extends Controller
{
    public function wsCarro($id=null){
        //Indica o tipo de retorno do método
        header("Content-type: application/json; charset=utf-8");

        //Verifica se o id foi (ou não) passado
        if ($id == null){
            $retorno = array("status" => "url incorreta",
                            "modelo" => null,
                            "ano" => null,
                            "preco" => null);
        }
        else {
            //Busca o registro
            $reg = Carro::find($id);

            //Se encontrado
            if (isset($reg)){
                $retorno = array("status" => "encontrado",
                "modelo" => $reg->modelo,
                "ano" => $reg->ano,
                "preco" => $reg->preco);
            }
            else {
                $retorno = array("status" => "inexistente",
                "modelo" => null,
                "ano" => null,
                "preco" => null);
            }
        }

        //Converte array para formato json
        echo json_encode($retorno, JSON_PRETTY_PRINT);
    }

    public function wsParceiros($id=null){
        //Indica o tipo de retorno do método
        header("Content-type: application/json; charset=utf-8");

        //Verifica se o id foi (ou não) passado
        if ($id == null){
            $retorno = array("status" => "url incorreta",
                            "nome" => null,
                            "marca" => null,
                            "cidade" => null,
                            "fone" => null);
        }
        else {
            //Busca o registro
            $reg = Parceiros::find($id);

            //Se encontrado
            if (isset($reg)){
                $retorno = array("status" => "encontrado",
                "nome" => $reg->nome,
                "marca" => $reg->marca,
                "cidade" => $reg->cidade,
                "fone" => $reg->fone);
            }
            else {
                $retorno = array("status" => "inexistente",
                    "nome" => null,
                    "marca" => null,
                    "cidade" => null,
                    "fone" => null);
            }
        }

        //Converte array para formato json
        echo json_encode($retorno, JSON_PRETTY_PRINT);
    }

    public function wsxml($id = null){
        //indica o tipo de retorno
        header("Content-type: application/xml");
        
        $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?>
                                    <carros></carros>');

        //verifica se o $id não foi passado
        if ($id == null){
            $item = $xml->addChild('carro');
            $item->addChild("status", "url incorreta");
            $item->addChild("modelo", null);
            $item->addChild("ano", null);
            $item->addChild("preco", null);
        } else {
            //busca o veículo cujo id foi passado como parâmetro
            $reg = Carro::find($id);
            
            //se existe
            if (isset($reg)){
                $item = $xml->addChild('carro');
                $item->addChild("status", "encontrado");
                $item->addChild("modelo", $reg->modelo);
                $item->addChild("ano", $reg->ano);
                $item->addChild("preco", $reg->preco);
            } else{
                $item = $xml->addChild('carro');
                $item->addChild("status", "Inexistente");
                $item->addChild("modelo", null);
                $item->addChild("ano", null);
                $item->addChild("preco", null);
            }
        }
        //retorna os dados no formato XML
        echo $xml->asXML();
    }

    public function listaxml($preco = null){
        //indica o tipo de retorno
        header("Content-type: application/xml");
        
        $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?>
                                    <carros></carros>');

        //verifica se o $id não foi passado
        if ($preco == null){
            $item = $xml->addChild('carro');
            $item->addChild("status", "url incorreta");
            $item->addChild("modelo", null);
            $item->addChild("ano", null);
            $item->addChild("preco", null);
        } else {
            //busca o veículo cujo preço seja menor que $preco
            $carros = Carro::where("preco", "<=", $preco)->get();
            
            //se existem carros até o preço passado como parâmetro
            if (count($carros) > 0){
                foreach($carros as $c){
                    $item = $xml->addChild('carro');
                    $item->addChild("status", "encontrado");
                    $item->addChild("modelo", $c->modelo);
                    $item->addChild("ano", $c->ano);
                    $item->addChild("preco", $c->preco);
                }
            } else{
                $item = $xml->addChild('carro');
                $item->addChild("status", "Inexistente");
                $item->addChild("modelo", null);
                $item->addChild("ano", null);
                $item->addChild("preco", null);
            }
        }
        //retorna os dados no formato XML
        echo $xml->asXML();
    }

    public function listaparceirosxml($filtro = null){
        //indica o tipo de retorno
        header("Content-type: application/xml");
        
        $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?>
                                    <carros></carros>');

        //verifica se o $id não foi passado
        if ($filtro == null){
            $item = $xml->addChild('parceiros');
            $item->addChild("status", "url incorreta");
            $item->addChild("nome", null);
            $item->addChild("marca", null);
            $item->addChild("cidade", null);
            $item->addChild("fone", null);
        } else {
            //busca o veículo cujo preço seja menor que $preco
            $parceiros = Parceiros::where("cidade", "=", $filtro)->get();
            
            //se existem carros até o preço passado como parâmetro
            if (count($parceiros) > 0){
                foreach($parceiros as $p){
                    $item = $xml->addChild('parceiros');
                    $item->addChild("status", "encontrado");
                    $item->addChild("nome", $p->nome);
                    $item->addChild("marca", $p->marca);
                    $item->addChild("cidade", $p->cidade);
                    $item->addChild("fone", $p->fone);
                }
            } else{
                $item = $xml->addChild('parceiros');
                $item->addChild("status", "Inexistente");
                $item->addChild("nome", null);
                $item->addChild("marca", null);
                $item->addChild("cidade", null);
                $item->addChild("fone", null);
            }
        }
        //retorna os dados no formato XML
        echo $xml->asXML();
    }
}
