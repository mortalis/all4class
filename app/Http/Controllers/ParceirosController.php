<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Parceiros;
use Illuminate\Support\Facades\Auth;

class ParceirosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::check()) {
            return redirect("/home");       
        }

                
        // $dados = Parceiros::all();
        $dados = Parceiros::paginate(5);


        return view('admin.parceiros_list', ['parceiros' => $dados]);
    }

    public function consulta(){
        return view ('admin.pconsulta');
    }

    public function search(Request $request){
        $cidade = $request->cidade;

        $filtro = array();

        if(!empty($cidade)){
            array_push($filtro, array('cidade', 'like', '%'.$cidade.'%'));
        }

        $parceiros = Parceiros::where($filtro)->orderBy('cidade')->paginate(5);

        return \PDF::loadView('admin.relparceiros', 
                            ['parceiros'=>$parceiros])->stream();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
