<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Mail\AvisoPromocional;


class EmailController extends Controller
{
    public function enviaEmail(){
        $destinatario = "michelcluz@gmail.com";
        
        Mail::to($destinatario)->send(new AvisoPromocional());
    }
}
