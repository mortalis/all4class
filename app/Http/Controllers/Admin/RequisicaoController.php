<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Requisicao;
use App\Instituicoes;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;


class RequisicaoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $requisicoes = Requisicao::orderBy('id', 'desc')->paginate(10);
        return view('admin.propostas_list', compact('requisicoes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /**
        $this->validate($request, [
            'nome' => 'required|min:10',
            'email' => 'required',
            'telefone' => 'required|min:9|max:40',
            'escola_id' => 'required'
        ]);
        **/
        //return $request->all();
        Requisicao::create($request->all());
        return redirect()->route('participe')->with('status', ' Requisição feita com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function responder($id)
    {
        $requisicoes = Requisicao::find($id);
        
        return view("admin.resposta", ["requisicoes" => $requisicoes]);
    }

    public function enviaEmail(Request $request)
    {
         
        $dados = $request->all();
        $requisicoes = Requisicao::find($dados["id"]);
        
        $resposta = $request["mensagem"];
        Mail::send("Mail.respostaproposta", ["requisicoes" => $requisicoes,
                                              "mensagem" => $resposta], 
                                              function ($message)use ($requisicoes) {
            $message->from("contato.revenda.herbie@gmail.com");
            $message->to($requisicoes->email);
            $message->subject("Resposta do contato  #".$requisicoes->id);
        });

        return redirect("admin/requisicoes");

    }
}
