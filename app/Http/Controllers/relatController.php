<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Carro;
use App\Parceiros;

class relatController extends Controller
{
    public function relcarros() {
        $carros = Carro::all();

        return \PDF::loadView('admin.relcarros', 
                            ['carros'=>$carros])->stream();
    }
    
    public function relparceiros() {
        $parceiros = Parceiros::all();

        return \PDF::loadView('admin.relparceiros', 
                            ['parceiros'=>$parceiros])->stream();
    }
}
