<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequisicaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requisicao', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nomeResponsavel', 40);
            $table->string('email', 40);
            $table->string('telefone', 40);
            $table->string('requisicao', 500);
            $table->integer('escola_id')->unsigned();
            $table->string('perfilAluno', 100);
            $table->integer('instituicao_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requisicao');
    }
}
