<?php

use Illuminate\Database\Seeder;

class InstituicoesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('instituicoes')->insert(['nome'=>'Polícia Militar',
        'telefone'=>'53 32211321',
        'endereco'=>"Rua tal, número 123"]);
        DB::table('instituicoes')->insert(['nome'=>'Polícia Civil',
        'telefone'=>'53 32211321',
        'endereco'=>"Rua tal, número 123"]);
        DB::table('instituicoes')->insert(['nome'=>'Bombeiros',
        'telefone'=>'53 32211321',
        'endereco'=>"Rua tal, número 123"]);
        DB::table('instituicoes')->insert(['nome'=>'Conselho Tutelar',
        'telefone'=>'53 32211321',
        'endereco'=>"Rua tal, número 123"]);
        DB::table('instituicoes')->insert(['nome'=>'Defensoria Pública',
        'telefone'=>'53 32211321',
        'endereco'=>"Rua tal, número 123"]);
    }
}
