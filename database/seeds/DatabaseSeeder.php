<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $this->call(BairroSeeder::class);
        $this->call(EscolaSeeder::class);
        $this->call(InstituicoesSeeder::class);
        
        
    }
}
