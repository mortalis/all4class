<?php

use Illuminate\Database\Seeder;

class EscolaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('escola')->insert([
            'nome' => 'E.E.E.F Visconde de Souza Soares',
            'bairro_id' => 1,
            'endereco' => 'Rua xxxxx, nº 123456'           
        ]);

        DB::table('escola')->insert([
            'nome' => 'Instituto Federal Sul-Riograndense',
            'bairro_id' => 1,
            'endereco' => 'Rua yyyy, nº 321654'           
        ]);

        DB::table('escola')->insert([
            'nome' => 'E.E.E.F Escola de Bairro Pobre',
            'bairro_id' => 5,
            'endereco' => 'Rua aaaaaa, nº 132645'           
        ]);

        DB::table('escola')->insert([
            'nome' => 'E.E.E.M Escola Senhor Fulano',
            'bairro_id' => 6,
            'endereco' => 'Rua ffff, nº 645798'           
        ]);
    }
}
