<?php

use Illuminate\Database\Seeder;

class BairroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bairro')->insert(['nome'=>'Fragata']);
        DB::table('bairro')->insert(['nome'=>'Centro']);
        DB::table('bairro')->insert(['nome'=>'Areal']);
        DB::table('bairro')->insert(['nome'=>'Porto']);
        DB::table('bairro')->insert(['nome'=>'Três Vendas']);
        DB::table('bairro')->insert(['nome'=>'Santa Terezinha']);
        DB::table('bairro')->insert(['nome'=>'Sanga Funda']);
        DB::table('bairro')->insert(['nome'=>'Vila Princesa']);
        DB::table('bairro')->insert(['nome'=>'Dunas']);
        DB::table('bairro')->insert(['nome'=>'Laranjal']);
        DB::table('bairro')->insert(['nome'=>'Getúlio Vargas']);
        DB::table('bairro')->insert(['nome'=>'Guabiroba']);
        DB::table('bairro')->insert(['nome'=>'Barro Duro']);
        DB::table('bairro')->insert(['nome'=>'Cascata']);
        DB::table('bairro')->insert(['nome'=>'Sítio Floresta']);
        DB::table('bairro')->insert(['nome'=>'Colônia Z3']);
        DB::table('bairro')->insert(['nome'=>'Pestano']);
        DB::table('bairro')->insert(['nome'=>'Cohab Tablada']);
        DB::table('bairro')->insert(['nome'=>'Cohab Lindoia']);
        DB::table('bairro')->insert(['nome'=>'Simões Lopes']);
        DB::table('bairro')->insert(['nome'=>'Cohab Fragata']);
        DB::table('bairro')->insert(['nome'=>'Santa Teresinha']);
        DB::table('bairro')->insert(['nome'=>'Arco Iris']);
        DB::table('bairro')->insert(['nome'=>'Navegantes Dois']);
        DB::table('bairro')->insert(['nome'=>'Nossa Senhora de Fátima']);

    }

}
