<?php

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix'=>'admin', 'namespace'=>'Admin'], function() {
   Route::get('/', function() { return view('admin.index'); });

   Route::get('resposta{id}', 'RequisicaoController@responder')->name('requisicoes.resposta');
   Route::post('resposta', 'RequisicaoController@enviaEmail')->name('resposta');

   Route::resource('escolas', 'EscolaController');
   Route::resource('bairros', 'BairroController');
   Route::resource('instituicoes', 'InstituicoesController');
   Route::resource('requisicoes', 'RequisicaoController');
   Route::resource('usuarios', 'UserController');
   Route::get('carrosgraf', 'CarroController@graf')
   ->name('carros.graf');
   Route::get('carrosdestaque/{id}', 'CarroController@destaque')
   ->name('carros.destaque');
   Route::resource('clientes', 'UUserController');
   
   
});

//Route::get('/admin', function() {
//    return view('admin.index');
//});
Auth::routes();



Route::get('home', 'HomeController@index')->name('home');
Route::get('/', function() { return view('index');});
Route::get('mapa', function() { return view('mapa');})->name("mapa");
Route::get('participe', function() { return view('participe');})->name("participe");
Route::get('relatorio', function() { return view('relatorio');})->name('relatorio');
Route::post('enviar_requisicao', 'RequisicaoController@store')->name("enviar_requisicao");
